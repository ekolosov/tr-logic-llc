// Каталог товаров в виде массива
export default [
  {
    id: '1',
    image: 'https://picsum.photos/500/300?image=15',
    title: 'Lorem ipsum dolor sit amet',
    price: '133,55',
  },
  {
    id: '2',
    image: 'https://picsum.photos/500/300?image=20',
    title: 'Lorem ipsum dolor sit amet',
    price: '505,44',
  },
  {
    id: '3',
    image: 'https://picsum.photos/500/300?image=25',
    title: 'Lorem ipsum dolor sit amet',
    price: '189',
  },
  {
    id: '4',
    image: 'https://picsum.photos/500/300?image=30',
    title: 'Lorem ipsum dolor sit amet',
    price: '2484,89',
  },
  {
    id: '5',
    image: 'https://picsum.photos/500/300?image=35',
    title: 'Lorem ipsum dolor sit amet',
    price: '404,15',
  },
  {
    id: '6',
    image: 'https://picsum.photos/500/300?image=40',
    title: 'Lorem ipsum dolor sit amet',
    price: '1598,45',
  },
];
