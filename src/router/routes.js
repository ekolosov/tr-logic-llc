// Список маршрутов сайтов с компонентами
import Home from '@/components/Home';

export default [
  {
    path: '/',
    name: 'Home',
    component: Home,
  },
  {
    path: '/cart',
    name: 'Cart',
    component: () => import('@/components/Cart'),
  },
  {
    path: '*',
    redirect: '/',
  },
];
