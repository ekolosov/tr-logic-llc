// Простейшее хранилище на основе localStorage со своими внутренними методами по управлению состоянием
const store = {
  state: {
    // Если корзина есть в localStorage - вернуть, иначе вернуть пустой массив
    cart: window.localStorage.cart ? JSON.parse(window.localStorage.cart) : [],
  },
  addToCart(element) {
    // Берем корзину
    const curCart = this.state.cart;
    // Ищем индекс товара в корзине (если есть item - то это товар из корзину, если нет, то из каталога
    const index = curCart.findIndex(i => (element.id && i.item.id === element.id) || (element.item && element.item.id && i.item.id === element.item.id));
    // Если нашли
    if (index > -1) {
      // Инкрементим количество
      ++curCart[index].quantity;
    } else if (!element.quantity && !element.item) {
      // Иначе, если это не товар из корзины, добавляем в корзину
      curCart.push({
        item: element,
        quantity: 1,
      });
    }

    // Сохраняем корзину в localStorage
    window.localStorage.setItem('cart', JSON.stringify(curCart));
  },
  removeFromCart(item) {
    // Берем корзину
    const curCart = this.state.cart;
    // Если в ней есть товары
    if (curCart.length > 0) {
      // Ищем индекс товара в корзине
      const index = curCart.findIndex(i => i.item.id === item.item.id);
      // Если нашли в корзине и количество больше 1
      if (index > -1 && item.quantity > 1) {
        // Декрементим количество
        --curCart[index].quantity;
      } else {
        // Иначе удаляем товар из корзины целиком
        curCart.splice(curCart.findIndex(i => i.item.id === item.item.id), 1);
      }
    }
    window.localStorage.setItem('cart', JSON.stringify(curCart));
  },
  getCart() {
    // Возвращаем текущую корзину
    return this.state.cart;
  },
  clearCart() {
    // Очищаем корзину - сначала локальную переменную для ускорения отрабоки computed свойств на его основе
    this.state.cart = [];
    // Теперь очищаем корзину в localStorage
    window.localStorage.setItem('cart', JSON.stringify([]));
  },
  cartSize() {
    // Возвращаем число товаров в корзине с учетом дублей одног товара
    // Сначала собираем в новый массив поле "количество", потом суммируем через reduce
    return this.state.cart.length > 0 ? this.state.cart.map(i => i.quantity).reduce((sum, current) => sum + current) : 0;
  },
};

export default store;
