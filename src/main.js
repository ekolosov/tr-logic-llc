import Vue from 'vue';
import '@/plugins/vuetify';
import router from '@/router';
import store from '@/store';
import App from '@/App';
import '@/assets/app.styl';

Vue.config.productionTip = false;

new Vue({
  // Установили роутинг с маршрутами
  router,
  data: {
    // Присвоили хранилище в глобальную data
    sharedStore: store,
  },
  render: h => h(App),
}).$mount('#app');
